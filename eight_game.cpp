/*
 * «Восьминашки» – упрощенный вариант известной головоломки «Пятнашки».
 * Восемь костяшек, пронумерованных от 1 до 8, расставлены по ячейкам игровой
 * доски 3 на 3, одна ячейка при этом остается пустой. За один ход разрешается
 * передвинуть одну из костяшек, расположенных рядом с пустой ячейкой, на
 * свободное место. Цель игры – для заданной начальной конфигурации игровой
 * доски за минимальное число ходов получить выигрышную конфигурацию Время -
 * O(n!). Память - O(n!).
 */

#include <algorithm>
#include <iostream>
#include <map>
#include <queue>
#include <vector>
#if defined HOME
#include "eighteen_game.h"
#endif
using std::map;
using std::queue;
using std::vector;
struct State {
  int field[3][3];
  int zero;
  State(long long code) {
    for (int cur = 0; cur < 9; cur++) {  // Раскодируем состояние
      long long cur_pos = code & 15LL;
      field[cur_pos / 3][cur_pos % 3] = cur;
      if (!cur) zero = cur_pos;
      code >>= 4;
    }
  }
  long long to_int() const {  // Кодируем состояние
    long long code = 0;
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        code += ((i * 3 + j * 1LL) << (field[i][j] * 4));
    return code;
  }
  long long up_code() {  // Сделаем ходы и сериализуем их.
    std::swap(field[zero / 3][zero % 3], field[zero / 3 - 1][zero % 3]);
    long long result = to_int();
    std::swap(field[zero / 3][zero % 3], field[zero / 3 - 1][zero % 3]);
    return result;
  }
  long long down_code() {
    std::swap(field[zero / 3][zero % 3], field[zero / 3 + 1][zero % 3]);
    long long result = to_int();
    std::swap(field[zero / 3][zero % 3], field[zero / 3 + 1][zero % 3]);
    return result;
  }
  long long left_code() {
    std::swap(field[zero / 3][zero % 3 - 1], field[zero / 3][zero % 3]);
    long long result = to_int();
    std::swap(field[zero / 3][zero % 3 - 1], field[zero / 3][zero % 3]);
    return result;
  }
  long long right_code() {
    std::swap(field[zero / 3][zero % 3], field[zero / 3][zero % 3 + 1]);
    long long result = to_int();
    std::swap(field[zero / 3][zero % 3], field[zero / 3][zero % 3 + 1]);
    return result;
  }

  bool parity_calc() {  // Подсчет четности перестановки
    vector<int> permut;
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        if (i % 2) {
          if (field[i][2 - j])
            permut.push_back(field[i][2 - j]);
        } else if (field[i][j]) {
          permut.push_back(field[i][j]);
        }
    int ans = 0;
    for (int i = 0; i < permut.size(); i++)
      for (int j = 0; j < i; j++)
        ans = (ans + (permut[i] < permut[j])) % 2;
    return ans;
  }
};

vector<char> eight_game_solve(State start, State finish);

#if not defined NOTMAIN
int main() {
  State start_state(0);
  State end_state(0);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      std::cin >> start_state.field[i][j];
      end_state.field[i][j] = i * 3 + j + 1;
    }
  end_state.field[2][2] = 0;
  if (start_state.parity_calc() != end_state.parity_calc()) {
    std::cout << -1;
    return 0;
  }
  vector<char> path = eight_game_solve(start_state, end_state);
  std::cout << path.size() << std::endl;
  for (auto it : path) std::cout << it;
  return 0;
}
#endif

vector<char> eight_game_solve(State start, State finish) {
  vector<char> ans;
  queue<long long> cur_queue;
  map<long long, int> distance;
  map<long long, std::pair<long long, char> > parent;
  distance[start.to_int()] = 0;
  parent[start.to_int()] = {-1, 0};
  long long finish_code = finish.to_int();
  cur_queue.push(start.to_int());
  while (!cur_queue.empty()) {
    long long cur_code = cur_queue.front();
    cur_queue.pop();
    if (cur_code == finish_code) break;
    State cur_state(cur_code);
    if (cur_state.zero > 2) { // Сделаем переходы.
      long long up_code = cur_state.up_code();
      if (!distance.count(up_code) ||
          distance[up_code] > distance[cur_code] + 1) {
        distance[up_code] = distance[cur_code] + 1;
        parent[up_code] = {cur_code, 'U'};
        cur_queue.push(up_code);
      }
    }
    if (cur_state.zero < 6) { 
      long long down_code = cur_state.down_code();
      if (!distance.count(down_code) ||
          distance[down_code] > distance[cur_code] + 1) {
        distance[down_code] = distance[cur_code] + 1;
        parent[down_code] = {cur_code, 'D'};
        cur_queue.push(down_code);
      }
    }
    if (cur_state.zero % 3) {
      long long left_code = cur_state.left_code();
      if (!distance.count(left_code) ||
          distance[left_code] > distance[cur_code] + 1) {
        distance[left_code] = distance[cur_code] + 1;
        parent[left_code] = {cur_code, 'L'};
        cur_queue.push(left_code);
      }
    }
    if (cur_state.zero % 3 < 2) {
      long long right_code = cur_state.right_code();
      if (!distance.count(right_code) ||
          distance[right_code] > distance[cur_code] + 1) {
        distance[right_code] = distance[cur_code] + 1;
        parent[right_code] = {cur_code, 'R'};
        cur_queue.push(right_code);
      }
    }
  }
  while (finish_code != start.to_int()) {
    ans.push_back(parent[finish_code].second);
    finish_code = parent[finish_code].first;
  }
  reverse(begin(ans), end(ans));
  return ans;
}

