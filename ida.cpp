/*
 * Задача D1. Пятнашки.
 * Написать алгоритм для решения игры в “пятнашки”.
 * Решением задачи является приведение к виду:
 * [ 1 2 3 4 ] [ 5 6 7 8 ] [ 9 10 11 12] [ 13 14 15 0 ],
 * где 0 задает пустую ячейку. Достаточно найти хотя бы какое-то решение.
 * Число перемещений костяшек не обязано быть минимальным.
 */
#include <algorithm>
#include <iostream>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
#if defined HOME
#include "eighteen_game.h"
#endif
#define INT_MAX 2147483647
using std::pair;
using std::queue;
using std::set;
using std::unordered_map;
using std::vector;
class IdaStarAlg {
  struct State {
    int field[4][4];
    int zero;
    State(unsigned long long code);
    unsigned long long to_int() const;
    void update_state(unsigned long long code);
    unsigned long long up_code();
    unsigned long long down_code();
    unsigned long long left_code();
    bool parity_calc();
    unsigned long long right_code();
  };
  State start;
  State finish;
  unsigned long long finish_state_code;
  unordered_map<unsigned long long, int> distance;
  unordered_map<unsigned long long, std::pair<unsigned long long, char>> parent;

  int calc_heuristics(unsigned long long state_code);
  pair<bool, int> search(unsigned long long state_code, size_t cur_distance,
                         size_t bound);

 public:
  IdaStarAlg(const vector<vector<int>> &start_field,
             const vector<vector<int>> &finish_field);
  bool is_solvable();
  vector<char> fifteen_solve();
};
#if not defined NOTMAIN
int main() {
  vector<vector<int>> start_state(4, vector<int>(4));
  vector<vector<int>> end_state(4, vector<int>(4));

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) {
      std::cin >> start_state[i][j];
      end_state[i][j] = i * 4 + j + 1;
    }
  end_state[3][3] = 0;
  IdaStarAlg instance(start_state, end_state);

  if (!instance.is_solvable()) {
    std::cout << -1;
    return 0;
  }
  vector<char> path = instance.fifteen_solve();
  std::cout << path.size() << std::endl;
  for (auto it : path) std::cout << it;
  return 0;
}
#endif
IdaStarAlg::State::State(unsigned long long code) {
  for (int cur = 0; cur < 16; cur++) {  // Раскодируем состояние
    unsigned long long cur_pos = code & 15LL;
    field[cur_pos >> 2][cur_pos & 3] = cur;
    if (!cur) zero = cur_pos;
    code >>= 4;
  }
}
void IdaStarAlg::State::update_state(unsigned long long code) {
  for (int cur = 0; cur < 16; cur++) {  // Раскодируем состояние
    unsigned long long cur_pos = code & 15LL;
    field[cur_pos >> 2][cur_pos & 3] = cur;
    if (!cur) zero = cur_pos;
    code >>= 4;
  }
}
unsigned long long IdaStarAlg::State::to_int() const {  // Кодируем состояние
  unsigned long long code = 0;
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      code |= ((i << 2 | j * 1LL) << (field[i][j] * 4));
  return code;
}
unsigned long long
IdaStarAlg::State::up_code() {  // Сделаем ходы и сериализуем их.
  std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) - 1][zero & 3]);
  unsigned long long result = to_int();
  std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) - 1][zero & 3]);
  return result;
}
unsigned long long IdaStarAlg::State::down_code() {
  std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) + 1][zero & 3]);
  unsigned long long result = to_int();
  std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) + 1][zero & 3]);
  return result;
}
unsigned long long IdaStarAlg::State::left_code() {
  std::swap(field[zero >> 2][(zero & 3) - 1], field[zero >> 2][zero & 3]);
  unsigned long long result = to_int();
  std::swap(field[zero >> 2][(zero & 3) - 1], field[zero >> 2][zero & 3]);
  return result;
}
unsigned long long IdaStarAlg::State::right_code() {
  std::swap(field[zero >> 2][zero & 3], field[zero >> 2][(zero & 3) + 1]);
  unsigned long long result = to_int();
  std::swap(field[zero >> 2][zero & 3], field[zero >> 2][(zero & 3) + 1]);
  return result;
}

bool IdaStarAlg::State::parity_calc() {  // Подсчет четности перестановки
  vector<int> permut;
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      if (i % 2) {
        if (field[i][3 - j]) permut.push_back(field[i][3 - j]);
      } else if (field[i][j]) {
        permut.push_back(field[i][j]);
      }
  int ans = 0;
  for (int i = 0; i < permut.size(); i++)
    for (int j = 0; j < i; j++) ans = (ans + (permut[i] < permut[j])) % 2;
  return ans;
}

pair<bool, int> IdaStarAlg::search(unsigned long long state_code,
                                   size_t cur_distance, size_t bound) {
  size_t f = cur_distance + calc_heuristics(state_code);
  if (f > bound) return {false, f};
  if (state_code == finish_state_code) return {true, f};
  int min_dist = INT_MAX;
  State cur_state(state_code);
  if (cur_state.zero > 3) {  // Сделаем переходы.
    parent[cur_state.up_code()] = {state_code, 'D'};
    auto current = search(cur_state.up_code(), cur_distance + 1, bound);
    if (current.first) return {true, current.second};
    min_dist = std::min(min_dist, current.second);
  }
  if (cur_state.zero < 12) {
    parent[cur_state.down_code()] = {state_code, 'U'};
    auto current = search(cur_state.down_code(), cur_distance + 1, bound);
    if (current.first) return {true, current.second};
    min_dist = std::min(min_dist, current.second);
  }
  if (cur_state.zero & 3) {
    parent[cur_state.left_code()] = {state_code, 'R'};
    auto current = search(cur_state.left_code(), cur_distance + 1, bound);
    if (current.first) return {true, current.second};
    min_dist = std::min(min_dist, current.second);
  }
  if ((cur_state.zero & 3) < 3) {
    parent[cur_state.right_code()] = {state_code, 'L'};
    auto current = search(cur_state.right_code(), cur_distance + 1, bound);
    if (current.first) return {true, current.second};
    min_dist = std::min(min_dist, current.second);
  }
  return {false, min_dist};
}

int IdaStarAlg::calc_heuristics(unsigned long long state_code) {
  State cur_state(state_code);
  int retValue(0);

  int row[] = {3, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3};
  int column[] = {3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2};
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) {
      if (cur_state.field[i][j] != 0) {
        retValue += abs(row[cur_state.field[i][j]] - i) +
                    abs(column[cur_state.field[i][j]] - j);
        for (int k = j + 1; k < 4; k++)
          if (row[cur_state.field[i][k]] == row[cur_state.field[i][j]] &&
              column[cur_state.field[i][k]] < column[cur_state.field[i][j]])
            retValue += 2;
        for (int k = i + 1; k < 4; k++) {
          if (column[cur_state.field[k][j]] == column[cur_state.field[i][j]] &&
              row[cur_state.field[k][j]] < row[cur_state.field[i][j]])
            retValue += 2;
        }
      }
    }
  return retValue;
}

vector<char> IdaStarAlg::fifteen_solve() {
  vector<char> ans;
  bool is_found = false;
  size_t bound = calc_heuristics(start.to_int());
  while (!is_found) {
    auto cur_ans = search(start.to_int(), 0, bound);
    if (cur_ans.first) break;
    bound = cur_ans.second;
  }
  unsigned long long finish_code = finish_state_code;
  while (finish_code != start.to_int()) {
    ans.push_back(parent[finish_code].second);
    finish_code = parent[finish_code].first;
  }
  reverse(begin(ans), end(ans));
  return ans;
}

bool IdaStarAlg::is_solvable() {
  return start.parity_calc() == finish.parity_calc();
}

IdaStarAlg::IdaStarAlg(const vector<vector<int>> &start_field,
                       const vector<vector<int>> &finish_field)
    : start(0), finish(0) {
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) {
      start.field[i][j] = start_field[i][j];
      finish.field[i][j] = finish_field[i][j];
      if (start_field[i][j] == 0) start.zero = 4 * i + j;
      if (finish_field[i][j] == 0) finish.zero = 4 * i + j;
    }
  finish_state_code = finish.to_int();
}
