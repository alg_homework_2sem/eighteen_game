/*
 * Задача D1. Пятнашки.
 * Написать алгоритм для решения игры в “пятнашки”.
 * Решением задачи является приведение к виду:
 * [ 1 2 3 4 ] [ 5 6 7 8 ] [ 9 10 11 12] [ 13 14 15 0 ],
 * где 0 задает пустую ячейку. Достаточно найти хотя бы какое-то решение.
 * Число перемещений костяшек не обязано быть минимальным.
*/
#pragma GCC optimize("O3")
#include <algorithm>
#include <iostream>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
#if defined HOME
#include "eighteen_game.h"
#endif
using std::queue;
using std::set;
using std::unordered_map;
using std::pair;
using std::vector;
struct State {
  int field[4][4];
  int zero;
  State(unsigned long long code) {
    for (int cur = 0; cur < 16; cur++) {  // Раскодируем состояние
      unsigned long long cur_pos = code & 15LL;
      field[cur_pos >> 2][cur_pos & 3] = cur;
      if (!cur) zero = cur_pos;
      code >>= 4;
    }
  }
  void update_state(unsigned long long code) {
    for (int cur = 0; cur < 16; cur++) {  // Раскодируем состояние
      unsigned long long cur_pos = code & 15LL;
      field[cur_pos >> 2][cur_pos & 3] = cur;
      if (!cur) zero = cur_pos;
      code >>= 4;
    }
  }
  unsigned long long to_int() const {  // Кодируем состояние
    unsigned long long code = 0;
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
        code |= ((i << 2 | j * 1LL) << (field[i][j] * 4));
    return code;
  }
  unsigned long long up_code() {  // Сделаем ходы и сериализуем их.
    std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) - 1][zero & 3]);
    unsigned long long result = to_int();
    std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) - 1][zero & 3]);
    return result;
  }
  unsigned long long down_code() {
    std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) + 1][zero & 3]);
    unsigned long long result = to_int();
    std::swap(field[zero >> 2][zero & 3], field[(zero >> 2) + 1][zero & 3]);
    return result;
  }
  unsigned long long left_code() {
    std::swap(field[zero >> 2][(zero & 3) - 1], field[zero >> 2][zero & 3]);
    unsigned long long result = to_int();
    std::swap(field[zero >> 2][(zero & 3) - 1], field[zero >> 2][zero & 3]);
    return result;
  }
  unsigned long long right_code() {
    std::swap(field[zero >> 2][zero & 3], field[zero >> 2][(zero & 3) + 1]);
    unsigned long long result = to_int();
    std::swap(field[zero >> 2][zero & 3], field[zero >> 2][(zero & 3) + 1]);
    return result;
  }

  bool parity_calc() {  // Подсчет четности перестановки
    vector<int> permut;
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
        if (i % 2) {
          if (field[i][3 - j]) permut.push_back(field[i][3 - j]);
        } else if (field[i][j]) {
          permut.push_back(field[i][j]);
        }
    int ans = 0;
    for (int i = 0; i < permut.size(); i++)
      for (int j = 0; j < i; j++) ans = (ans + (permut[i] < permut[j])) % 2;
    return ans;
  }
};

vector<char> fifteen_solve(State start, State finish);
int calc_heuristics(unsigned long long state_code);
#if not defined NOTMAIN
int main() {
  State start_state(0);
  State end_state(0);
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) {
      std::cin >> start_state.field[i][j];
      end_state.field[i][j] = i * 4 + j + 1;
    }
  end_state.field[3][3] = 0;
  if (start_state.parity_calc() != end_state.parity_calc()) {
    std::cout << -1;
    return 0;
  }
  vector<char> path = fifteen_solve(start_state, end_state);
  std::cout << path.size() << std::endl;
  for (auto it : path) std::cout << it;
  return 0;
}
#endif

int calc_heuristics(unsigned long long state_code) {
  State cur_state(state_code);
  int retValue(0);

  int row[] = {3, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3};
  int column[] = {3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2};
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) {
      if (cur_state.field[i][j] != 0) {
        retValue += abs(row[cur_state.field[i][j]] - i) +
                    abs(column[cur_state.field[i][j]] - j);
        for (int k = j + 1; k < 4; k++)
          if (row[cur_state.field[i][k]] == row[cur_state.field[i][j]] &&
              column[cur_state.field[i][k]] < column[cur_state.field[i][j]])
            retValue += 2;
        for (int k = i + 1; k < 4; k++) {
          if (column[cur_state.field[k][j]] == column[cur_state.field[i][j]] &&
              row[cur_state.field[k][j]] < row[cur_state.field[i][j]])
            retValue += 2;
        }
      }
    }
  return retValue;
}

vector<char> fifteen_solve(State start, State finish) {
  vector<char> ans;
  set<std::pair<int, unsigned long long> > cur_queue;
  unordered_map<unsigned long long, int> distance;
  unordered_map<unsigned long long, std::pair<unsigned long long, char> >
      parent;
  distance[start.to_int()] = 0;
  parent[start.to_int()] = {-1, 0};
  unsigned long long finish_code = finish.to_int();
  cur_queue.insert({calc_heuristics(start.to_int()), start.to_int()});
  unsigned long long cur_code;
  unsigned long long up_code;
  unsigned long long down_code;
  unsigned long long left_code;
  unsigned long long right_code;
  State cur_state(start.to_int());
  while (!cur_queue.empty()) {
    cur_code = cur_queue.begin()->second;
    cur_queue.erase(cur_queue.begin());
    if (cur_code == finish_code) break;
    cur_state.update_state(cur_code);
    if (cur_state.zero > 3) {  // Сделаем переходы.
      up_code = cur_state.up_code();
      if (!distance.count(up_code) ||
          distance[up_code] > distance[cur_code] + 1) {
        int heuristics = calc_heuristics(up_code);
        cur_queue.erase({heuristics, up_code});
        distance[up_code] = distance[cur_code] + 1;
        parent[up_code] = {cur_code, 'D'};
        cur_queue.insert({heuristics, up_code});
      }
    }
    if (cur_state.zero < 12) {
      down_code = cur_state.down_code();
      if (!distance.count(down_code) ||
          distance[down_code] > distance[cur_code] + 1) {
        int heuristics = calc_heuristics(down_code);
        cur_queue.erase({heuristics, down_code});
        distance[down_code] = distance[cur_code] + 1;
        parent[down_code] = {cur_code, 'U'};
        cur_queue.insert({heuristics, down_code});
      }
    }
    if (cur_state.zero & 3) {
      left_code = cur_state.left_code();
      if (!distance.count(left_code) ||
          distance[left_code] > distance[cur_code] + 1) {
        int heuristics = calc_heuristics(left_code);
        cur_queue.erase({heuristics, left_code});
        distance[left_code] = distance[cur_code] + 1;
        parent[left_code] = {cur_code, 'R'};
        cur_queue.insert({heuristics, left_code});
      }
    }
    if ((cur_state.zero & 3) < 3) {
      right_code = cur_state.right_code();
      if (!distance.count(right_code) ||
          distance[right_code] > distance[cur_code] + 1) {
        int heuristics = calc_heuristics(right_code);
        cur_queue.erase({heuristics, right_code});
        distance[right_code] = distance[cur_code] + 1;
        parent[right_code] = {cur_code, 'L'};
        cur_queue.insert({heuristics, right_code});
      }
    }
  }
  while (finish_code != start.to_int()) {
    ans.push_back(parent[finish_code].second);
    finish_code = parent[finish_code].first;
  }
  reverse(begin(ans), end(ans));
  return ans;
}

